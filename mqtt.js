// Shinobi to MQTT bridge
// Version 1.0
// Copyright (C) 2019 Oleg Vivtash <o@vivtash.net>
// https://gitlab.com/ovivtash/shinobi-mqtt

const moment = require('moment');
const mqtt = require('mqtt');

const config = {
  verbose: false,
  url: 'mqtt://localhost:1883',
  topic: 'shinobi'
  //TODO
  // trigger: true,
  // no_trigger: true,
};

let mqttClient;

const timestamp = () => moment().format('YYYY-MM-DD HH:mm:ss');
const log = (wat) => console.log(`${timestamp()} MQTT:`, wat);
const verboseLog = (wat) => {
  if(config.verbose) {
    console.log(`${timestamp()} MQTT:`, wat);
  }
};

function motionEventHandler(d, filter){
  verboseLog(`${d.name} event on ${d.id}`);
  if(!mqttClient.connected) {
    verboseLog('No connection to broker, skipping');
    return;
  }

  const userGroupKey = d.ke;
  const monitorId = d.id || d.mon.mid;
  const evt = {
    key: d.key,
    name: d.name,
    plugin: d.details.plug,
    reason: d.details.reason,
    confidence: d.details.confidence,
    ts: d.currentTimestamp
  };
  const topic = `${config.topic}/${userGroupKey}/${monitorId}/${d.f}`;
  mqttClient.publish(topic, JSON.stringify(evt), null, () => {
    verboseLog(`Sent ${d.name} event #${d.id} to ${topic}`);
  });
}

module.exports = (s, shinobiConfig, lang, app, io) => {
  Object.entries(shinobiConfig.mqtt || {}).filter(([k, v]) => v !== undefined).forEach(([k, v]) => config[k] = v);
  mqttClient = mqtt.connect(config.url, {
    clientId: `shinobi_${Math.random().toString(16).substr(2, 8)}`,
    reconnectPeriod: 10000 // 10 seconds
  });
  mqttClient.on('connect', () => log('Connected to broker'));
  mqttClient.on('reconnect', () => log('Trying to reconnect to broker...'));
  //mqttClient.on('close', () => log('Broker connection closed'));
  mqttClient.on('disconnect', () => log('Disconnected from broker'));
  mqttClient.on('offline', () => log('Gone offline'));
  mqttClient.on('error', (e) => log(e));

  s.onEventTrigger(motionEventHandler);
  s.onProcessExit(() => mqttClient.end());
  log('Bridge loaded');
};
